# Pomodoro

This project contains the development of a pomodoro digital timer, used in the well-known Pomodoro Technique.
The pomodoro technique has periods of work and rest, generally 25 minutes to work and a 5 minutes break. But these times can be adapted to the preferences of the users.

## Background

In the last months I've been using the Pomodoro technique while studying or working. Usually I play YouTube timer videos, but sometimes it's awkward to have an extra tab in Opera.

Why should I spend five minutes buying a pomodoro timer and start working if I can spend a few weeks making one? That's the alma máter of this project.

## Hardware

- WeMos D1 Mini (ESP 8266).
- 16x2 LCD screen (with I2C adapter).
- Button (start - restart session).
- Passive Buzzer to alert period changes.
